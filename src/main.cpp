#include <SDL.h>
#include <graphics.hpp>
#include <menu.hpp>
#include <iostream>

using namespace std;

int main(int argc, char** argv) {
    Graphics* g = new Graphics();

    if (!g->init()) {
        return 1;
    }

    g->set_state((Menu*) new Menu());
    g->loop();

    g->close();
}
