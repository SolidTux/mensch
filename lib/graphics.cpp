#include <SDL.h>
#include <SDL_ttf.h>
#include <iostream>
#include <state.hpp>
#include <graphics.hpp>

using namespace std;

bool Graphics::init() {
    if (SDL_Init(SDL_INIT_VIDEO) == -1) {
        cerr << SDL_GetError() << endl;
        return false;
    }

    if (TTF_Init() == -1) {
        cerr << TTF_GetError() << endl;
        return false;
    }

    win = SDL_CreateWindow("Mensch Ärgere dich nicht!",
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (win == nullptr) {
        cerr << SDL_GetError() << endl;
        return false;
    }

    ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);
    if (ren == nullptr) {
        cerr << SDL_GetError() << endl;
        return false;
    }
    SDL_SetRenderDrawColor(ren, 0xFF, 0xFF, 0xFF, 0xFF);

    font = TTF_OpenFont("res/sans.ttf", FONT_SIZE);
    if (font == nullptr) {
        cerr << TTF_GetError() << endl;
    }

    sur = SDL_GetWindowSurface(win);
    return true;
}

void Graphics::loop() {
    running = true;
    SDL_Event e;
    while (running) {
        SDL_SetRenderDrawColor(ren, 0x00, 0x77, 0x00, 0xFF);
        SDL_RenderClear(ren);
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                running = false;
            } else {
                state->process_event(this, e);
            }
        }
        state->update(this);
        update();
    }
}

void Graphics::update() {
    SDL_RenderPresent(ren);
}

void Graphics::quit() {
    running = false;
}

void Graphics::close() {
    TTF_Quit();
    SDL_DestroyRenderer(ren);
    SDL_DestroyWindow(win);
    SDL_Quit();
}

void Graphics::set_state(State* s) {
    state = s;
    s->init(this);
    update();
}
