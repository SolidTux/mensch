#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <SDL.h>
#include <SDL_ttf.h>
#include <state.hpp>
#include <graphics.hpp>
#include <menu.hpp>
#include <quit.hpp>

using namespace std;

Menu::Menu() {
    entries.reserve(2);
    MenuEntry test = MenuEntry("Test");
    //test.next = new Menu();
    MenuEntry quit = MenuEntry("Quit");
    quit.next = new Quit();
    entries.push_back(test);
    entries.push_back(quit);
}

void Menu::init(Graphics* g) {
    update(g);
}

void Menu::update(Graphics* g) {
    for (int i=0; i<entries.size(); i++) {
        SDL_Rect r;
        r.x = static_cast<int>(0.5*(SCREEN_WIDTH-ENTRY_WIDTH));
        r.y = static_cast<int>(0.5*SCREEN_HEIGHT + (i-0.5*entries.size())*(ENTRY_HEIGHT+ENTRY_PADDING));
        r.w = ENTRY_WIDTH;
        r.h = ENTRY_HEIGHT;
        SDL_Color t_col;
        if (i == selected) {
            SDL_SetRenderDrawColor(g->ren, 0xFF, 0x00, 0x00, 0xFF);
            t_col = {0xFF, 0xFF, 0xFF, 0xFF};
        } else {
            SDL_SetRenderDrawColor(g->ren, 0x00, 0x00, 0xFF, 0xFF);
            t_col = {0xFF, 0xFF, 0xFF, 0xFF};
        }
        SDL_RenderFillRect(g->ren, &r);
        SDL_Surface* t_sur = TTF_RenderText_Solid(g->font,
                entries[i].title.c_str(), t_col);
        if (t_sur == nullptr) {
            cerr << TTF_GetError() << endl;
        }
        SDL_Texture* t_tex = SDL_CreateTextureFromSurface(g->ren, t_sur);
        SDL_Rect t_r;
        t_r.w = t_sur->w;
        t_r.h = t_sur->h;
        t_r.x = r.x + 0.5*(r.w - t_r.w);
        t_r.y = r.y + 0.5*(r.h - t_r.h);
        SDL_RenderCopy(g->ren, t_tex, NULL, &t_r);
        SDL_DestroyTexture(t_tex);
        TTF_CloseFont(font);
    }
}

void Menu::process_event(Graphics* g, SDL_Event e) {
    if (e.type == SDL_KEYDOWN) {
        switch (e.key.keysym.sym) {
            case SDLK_q:
                g->quit();
                break;
            case SDLK_ESCAPE:
                g->quit();
                break;
            case SDLK_UP:
                if (selected > 0) selected--;
                break;
            case SDLK_DOWN:
                if (selected < static_cast<int>(entries.size()-1)) selected++;
                break;
            case SDLK_RETURN:
                State* n = entries[selected].next;
                if (n != nullptr) g->set_state(n);
                break;
        }
    }
}

MenuEntry::MenuEntry() {
    title = "Default";
}

MenuEntry::MenuEntry(string t) {
    title = t;
}
