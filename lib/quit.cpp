#include <graphics.hpp>
#include <state.hpp>
#include <quit.hpp>

void Quit::init(Graphics* g) {
    g->quit();
}
