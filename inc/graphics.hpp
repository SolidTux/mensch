#ifndef GRAPHICS_HPP
#define GRAPHICS_HPP

#include <SDL.h>

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
#define FONT_SIZE static_cast<int>(0.08*SCREEN_HEIGHT)

using namespace std;

class State;

class Graphics {
    private:
        State* state;
        bool running = false;
    public:
        SDL_Window* win;
        SDL_Surface* sur;
        SDL_Renderer* ren;
        TTF_Font* font;
        bool init();
        void quit();
        void close();
        void update();
        void set_state(State*);
        void loop();
};

#endif
