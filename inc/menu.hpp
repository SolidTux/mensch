#ifndef MENU_HPP
#define MENU_HPP

#include <SDL.h>
#include <state.hpp>
#include <graphics.hpp>
#include <string>
#include <vector>

#define ENTRY_WIDTH static_cast<int>(0.5*SCREEN_WIDTH)
#define ENTRY_HEIGHT static_cast<int>(0.1*SCREEN_HEIGHT)
#define ENTRY_PADDING static_cast<int>(0.05*SCREEN_HEIGHT)

class MenuEntry {
    public:
        MenuEntry();
        MenuEntry(string);
        string title;
        State* next;
};

class Menu: public State {
    private:
        int selected = -1;
    public:
        Menu();
        Graphics* g;
        vector<MenuEntry> entries;
        void init(Graphics*);
        void update(Graphics*);
        void process_event(Graphics*, SDL_Event);
};

#endif
