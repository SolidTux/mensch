#include <graphics.hpp>
#include <state.hpp>

class Quit: public State {
    public:
        Quit() {};
        void init(Graphics*);
        void update(Graphics*) {};
        void process_event(Graphics*, SDL_Event) {};
};

