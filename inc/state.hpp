#ifndef LOGIC_HPP
#define LOGIC_HPP

#include <SDL.h>

class Graphics;

class State {
    public:
        virtual void init(Graphics*) {};
        virtual void update(Graphics*) {};
        virtual void process_event(Graphics*, SDL_Event) {};
};

#endif
